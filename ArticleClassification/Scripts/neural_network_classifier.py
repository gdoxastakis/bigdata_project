import numpy as np
import tensorflow as tf
import ArticleClassification.Dataset as Dataset
from sklearn import metrics
from sklearn.model_selection import train_test_split
import os


def perform_classification():
    train_df = Dataset.load_train(os.path.join('..', '..', 'Datasets', 'train_set.csv'))
    test_df = Dataset.load_test(os.path.join('..', '..', 'Datasets', 'test_set.csv'))
    train_labels = train_df['Category'].values.codes
    train_data, test_data = Dataset.preprocess(train_df, test_df, method='BoW_TFIDF')

    with tf.device('/device:GPU:0'):
        train_labels = tf.keras.utils.to_categorical(train_labels, 5)
        train_data, X_test, train_labels, y_test = train_test_split(train_data, train_labels, test_size=0.1)

        model = tf.keras.Sequential()
        model.add(tf.keras.layers.Dense(units=256, activation='tanh', input_dim=train_data.shape[1]))
        model.add(tf.keras.layers.Dropout(0.2))
        model.add(tf.keras.layers.Dense(256, activation='tanh'))
        model.add(tf.keras.layers.Dropout(0.2))
        model.add(tf.keras.layers.Dense(5, activation='softmax'))

        model.compile(optimizer="Adam", loss="categorical_crossentropy", metrics=["accuracy"])
        model.summary()

        stop = tf.keras.callbacks.EarlyStopping(monitor='val_acc', patience=1, min_delta=0.01)
        log = tf.keras.callbacks.TensorBoard()

        model.fit(x=train_data, y=train_labels, epochs=50, batch_size=100, validation_split=0.2, callbacks=[stop])
        test_pred = np.argmax(model.predict(test_data), axis=1)
        test_df['Category'] = np.array(train_df['Category'].cat.categories.tolist())[np.asarray(test_pred)]
        test_df = test_df.drop(['Title', 'Content'], axis=1)
        test_df.to_csv('NN_testSet_Categories.csv', index=False)
        p_test = np.argmax(model.predict(X_test), axis=1)
        y_test = np.argmax(y_test, axis=1)
        acc = metrics.accuracy_score(y_test, p_test)
        prec = metrics.precision_score(y_test, p_test, average='macro')
        rec = metrics.recall_score(y_test, p_test, average='macro')
        f1 = metrics.f1_score(y_test, p_test, average='macro')

        nn_results = {'Accuracy': acc, 'Precision': prec, 'Recall': rec, 'F-Measure': f1}
        return nn_results


if __name__ == '__main__':
    print(perform_classification())
