import os
import pandas as pd
import numpy as np
import ArticleClassification.Dataset as Dataset
import scipy.spatial
from sparselsh import LSH

Threshold = 0.7

if __name__ == '__main__':
    train_df = Dataset.load_train(os.path.join('..', '..', 'Datasets', 'train_set.csv'))
    test_df = Dataset.load_test(os.path.join('..', '..', 'Datasets', 'test_set.csv'))
    train_vec, test_vec = Dataset.preprocess(train_df, test_df)
    lsh = LSH(16,
              train_vec.shape[1],
              num_hashtables=1,
              storage_config={"dict": None})
    for ix in range(train_vec.shape[0]):
        x = train_vec.getrow(ix)
        lsh.index(x)

    train_list = train_vec.toarray().tolist()
    duplicates = []
    for ix in range(train_vec.shape[0]):
        x = train_vec.getrow(ix)
        points = lsh.query(x, num_results=2)
        if len(points) == 2:
            euc_distance = points[1][1]
            doc1 = points[0][0].toarray()
            doc2 = points[1][0].toarray()
            cos_similarity = 1 - scipy.spatial.distance.cosine(doc1, doc2)
            if cos_similarity > Threshold:
                duplicates.append([train_df['Id'].values[train_list.index(doc1.flatten().tolist())],
                                   train_df['Id'].values[train_list.index(doc2.flatten().tolist())], cos_similarity])

    df = pd.DataFrame(data=duplicates, columns=['Document_ID1', 'Document_ID2', 'Similarity'])
    df = df.iloc[np.where((np.not_equal(df['Document_ID1'].values, df['Document_ID2'].values)))[0]]
    df = df.drop_duplicates('Similarity')
    df = df.reset_index()
    df.to_csv('Duplicates.csv', index=False)
