import ArticleClassification.Dataset as dataset
import os
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt
import numpy as np


def create_circular_mask(h, w, center=None, radius=None):

    if center is None:  # use the middle of the image
        center = [int(w/2), int(h/2)]
    if radius is None:  # use the smallest distance between the center and image walls
        radius = min(center[0], center[1], w-center[0], h-center[1])

    y, x = np.ogrid[:h, :w]
    dist_from_center = np.sqrt((x - center[0])**2 + (y-center[1])**2)

    mask = dist_from_center <= radius
    return np.invert(mask)*255


if __name__ == '__main__':
    if not os.path.exists('WordClouds'):
        os.mkdir('WordClouds')

    sw = set(STOPWORDS)
    sw.add('said')
    sw.add('say')
    sw.add('says')
    sw.add('will')

    data = dataset.load_train(os.path.join('..', '..', 'Datasets', 'train_set.csv'))
    groups = data.groupby('Category')
    for c in data['Category'].cat.categories.tolist():
        category = groups.get_group(c)
        text = category.Content.sum()

        print("Creating WordCloud for category " + c + " ...")

        # Create and generate a word cloud image:
        wordcloud = WordCloud(stopwords=sw, background_color="white",
                              mask=create_circular_mask(1000, 1000), margin=10).generate(text)

        # Display the generated image:
        plt.imshow(wordcloud, interpolation='bilinear')
        plt.axis("off")
        plt.title(c)

        plt.savefig(fname=os.path.join('WordClouds', c+'.png'), dpi=300)
