from ArticleClassification.Scripts.svm_classifier import perform_classification as SVM_Classification
from ArticleClassification.Scripts.random_forest_classifier import perform_classification as RF_Classification
from ArticleClassification.Scripts.neural_network_classifier import perform_classification as NN_Classification
import numpy as np
import pandas as pd
from collections import OrderedDict

if __name__ == '__main__':

    metrics = ['Accuracy', 'Precision', 'Recall', 'F-Measure']

    # Computations
    print("Computing results ...")

    svm_bow = SVM_Classification("BoW")
    svm_svd = SVM_Classification("SVD")
    svm_w2v = SVM_Classification("Word2Vec")

    rf_bow = RF_Classification("BoW")
    rf_svd = RF_Classification("SVD")
    rf_w2v = RF_Classification("Word2Vec")

    neural_network = NN_Classification()

    print("Computations completed")

    data_dict = OrderedDict([('Statistic Measure', metrics),
                             ('SVM (BoW)', list(svm_bow.values())),
                             ('Random Forest (BoW)', list(rf_bow.values())),
                             ('SVM (SVD)', list(svm_svd.values())),
                             ('Random Forest (SVD)', list(rf_svd.values())),
                             ('SVM (W2V)', list(svm_w2v.values())),
                             ('Random Forest (W2V)', list(rf_w2v.values())),
                             ('Neural Network', list(neural_network.values()))])
    df = pd.DataFrame.from_dict(data_dict)

    print(df)
    df.to_csv("EvaluationMetric_10fold.csv")
    print("Results file (EvaluationMetric_10fold.csv) exported successfully")
