import sys
import os
import warnings
import sklearn.exceptions
from time import time
import ArticleClassification.Dataset as Dataset
from sklearn.model_selection import StratifiedKFold
from sklearn import svm
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score


def perform_classification(pr_method):

    warnings.filterwarnings("ignore", category=sklearn.exceptions.UndefinedMetricWarning)

    print("\n\nPerform training, prediction and cross-validation with SVM using " + pr_method + " ...\n", end="")
    sys.stdout.flush()
    t0 = time()

    # Load datasets
    train_df = Dataset.load_train(os.path.join('..', '..', 'Datasets', 'train_set.csv'))
    test_df = Dataset.load_test(os.path.join('..', '..', 'Datasets', 'test_set.csv'))
    train_labels = train_df['Category'].values.codes
    train_data, test_data = Dataset.preprocess(train_df, test_df, pr_method)

    # Metrics
    metrics = ['Accuracy', 'Precision', 'Recall', 'F-Measure']

    # KFold cross-validation
    # The folds are made by preserving the percentage of samples for each class.
    skf = StratifiedKFold(n_splits=10, shuffle=True)

    svm_results = {}
    for metric in metrics:
        svm_results[metric] = 0

    current_fold = 1
    for train_i, test_i in skf.split(train_data, train_labels):

        print("Fold "+str(current_fold))

        x_train, x_test = train_data[train_i], train_data[test_i]
        y_train, y_test = train_labels[train_i], train_labels[test_i]

        #clf = svm.SVC(kernel='rbf', gamma='scale')
        clf = svm.LinearSVC()
        clf.fit(x_train, y_train)

        y_pred = clf.predict(x_test)
        svm_results['Accuracy'] += accuracy_score(y_test, y_pred)
        svm_results['Precision'] += precision_score(y_test, y_pred, average='weighted')
        svm_results['Recall'] += recall_score(y_test, y_pred, average='weighted')
        svm_results['F-Measure'] += f1_score(y_test, y_pred, average='weighted')
        current_fold += 1

    print("\nCompleted in %0.3f sec\n" % (time() - t0))

    print("###### Metrics ######")
    for metric in metrics:
        svm_results[metric] = svm_results[metric] / 10.0
        print(metric + ": %.3f" % svm_results[metric])

    return svm_results


if __name__ == '__main__':

    perform_classification("BoW")
    perform_classification("BoW_TFIDF")
    perform_classification("SVD")
    perform_classification("Word2Vec")
