import re

import gensim
import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.decomposition import TruncatedSVD


def load_train(path):
    df = pd.read_csv(path, sep='\t')
    df = df.drop(columns=['RowNum'])
    df['Category'] = df['Category'].astype('category')
    return df


def load_test(path):
    df = pd.read_csv(path, sep='\t')
    df = df.drop(columns=['RowNum'])
    return df


def preprocess(train_dataframe, test_dataframe, method='BoW'):
    corpus = train_dataframe['Content'].values.tolist()
    if method in ['BoW', 'SVD']:
        vectorizer = CountVectorizer(stop_words='english')
        train_vector = vectorizer.fit_transform(corpus)
        test_vector = vectorizer.transform(test_dataframe['Content'].values.tolist())

        if method == 'SVD':
            vectorizer = TruncatedSVD(n_components=3000, n_iter=1)
            train_vector = vectorizer.fit_transform(train_vector)
            test_vector = vectorizer.transform(test_vector)

            print("Variance_Ratio: " + str(vectorizer.explained_variance_ratio_.sum()))
            print("SVD train matrix size: " + str(train_vector.shape))
            print("SVD test matrix size:  " + str(test_vector.shape) + "\n")
        else:
            print("BoW CountVectorizer train matrix size: " + str(train_vector.shape))
            print("BoW CountVectorizer test matrix size:  " + str(test_vector.shape) + "\n")

        return train_vector, test_vector

    elif method == 'BoW_TFIDF':
        # Article used as reference :
        # https://towardsdatascience.com/multi-class-text-classification-with-scikit-learn-12f1e60e0a9f

        # max_df : Ignore terms that have a document frequency strictly higher than the given threshold
        # sublinear_df is set to True to use a logarithmic form for frequency.
        # norm is set to l2, to ensure all our feature vectors have a Euclidian norm of 1
        # ngram_range is set to (1, 2) to indicate that we want to consider both unigrams and bigrams

        vectorizer = TfidfVectorizer(sublinear_tf=True, min_df=5, norm='l2', ngram_range=(1, 2), stop_words='english', max_df=0.5)
        train_vector = vectorizer.fit_transform(corpus)
        test_vector = vectorizer.transform(test_dataframe['Content'].values.tolist())

        print("BoW TFIDF train matrix size: " + str(train_vector.shape))
        print("BoW TFIDF test matrix size:  " + str(test_vector.shape) + "\n")

        return train_vector, test_vector

    elif method == 'Word2Vec':

        # https://medium.com/scaleabout/a-gentle-introduction-to-doc2vec-db3e8c0cce5e

        # According to Kaggle site:
        # To train Word2Vec it is better not to remove stop words because the algorithm relies
        # on the broader context of the sentence in order to produce high-quality word vectors

        # Generate tagged documents from our corpus
        tagged_documents = []
        for i in range(len(corpus)):
            # Every document content in corpus should be converted to words (lower case) separated by spaces
            transformed_content = re.sub("[^\w]", " ", corpus[i].lower())
            wordlist = transformed_content.split()
            tagged_documents.append(gensim.models.doc2vec.TaggedDocument(wordlist, ['CorpusSentence' + str(i)]))

        # Generate and train our model (tagged_documents are provided , so training starts automatically)
        word2vec = gensim.models.Doc2Vec(tagged_documents, vector_size=300, workers=4)

        train_vector = []
        for i in range(len(tagged_documents)):
            train_vector.append(word2vec.infer_vector(tagged_documents[i][0]))
        train_vector = np.array(train_vector)

        test_vector = []
        test_contents = test_dataframe['Content'].values.tolist()
        for i in range(len(test_contents)):
            transformed_test_content = re.sub("[^\w]", " ", test_contents[i].lower()).split()
            test_vector.append(word2vec.infer_vector(transformed_test_content))
        test_vector = np.array(test_vector)

        print("Word2Vec train matrix size: " + str(train_vector.shape))
        print("Word2Vec test matrix size: " + str(test_vector.shape) + "\n")

        return train_vector, test_vector

    else:
        raise NotImplementedError
